import java.util.HashMap;
import java.util.Map;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;

public class DynamoDBHelper {
	private static DynamoDB dynamoDb;
    private static String DYNAMODB_USER_DATA_TABLE_NAME = "user-data";
    private static String DYNAMODB_EVENT_DATA_TABLE_NAME = "event-data";
    private static Regions REGION = Regions.US_EAST_1;
    
    static void initDynamoDbClient() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setRegion(Region.getRegion(REGION));
        dynamoDb = new DynamoDB(client);
    }
    
	static void insertNewEvent(String userId, String subscriptionId, String eventId, String eventData) 
	{
		Table table = dynamoDb.getTable(DYNAMODB_EVENT_DATA_TABLE_NAME);
		Item item = new Item()
					.withPrimaryKey("event-id", eventId)
					.withString("subscription-id", subscriptionId)
					.withString("user-id", userId)
					.with("event-data", eventData);
		table.putItem(item);
	}
	static void updateExistingEvent(String userId, String subscriptionId, String eventId, String eventData)
	{
		Table table = dynamoDb.getTable(DYNAMODB_EVENT_DATA_TABLE_NAME);
		
		Map<String, String> expressionAttributeNames = new HashMap<String, String>();
		expressionAttributeNames.put("#R", "event-data");

		Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
		expressionAttributeValues.put(":val1", eventData);
		
		table.updateItem("event-id", eventId,"set #R = :val1", expressionAttributeNames, expressionAttributeValues);
	}
	static void deleteExistingEvent(String userId, String subscriptionId, String eventId, String eventData)
	{
		Table table = dynamoDb.getTable(DYNAMODB_EVENT_DATA_TABLE_NAME);
		table.deleteItem("event-id", eventId);
	}
	
	static String getRefreshToken(String userId)
	{
		Table table = dynamoDb.getTable(DYNAMODB_USER_DATA_TABLE_NAME);
        Item item = table.getItem("user-id", userId, "refresh-token", null);
        String refreshToken = item.getString("refresh-token");
        return refreshToken;
	}

	static void updateRefreshToken(String userId, String refreshToken) 
	{
		Table table = dynamoDb.getTable(DYNAMODB_USER_DATA_TABLE_NAME);
		
		Map<String, String> expressionAttributeNames = new HashMap<String, String>();
		expressionAttributeNames.put("#R", "refresh-token");

		Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
		expressionAttributeValues.put(":val1", refreshToken);
		
		table.updateItem("user-id", userId,"set #R = :val1", expressionAttributeNames, expressionAttributeValues);
	}

}
