import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class NotificationReceiver implements RequestHandler<Object, String>
{
	public String handleRequest(Object event, Context context)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		ObjectMapper mapper = new ObjectMapper();
		LambdaLogger logger = context.getLogger();
		logger.log("EVENT: " + gson.toJson(event));
		String json = gson.toJson(event);
		Map<String, Object> map = null;
        try {
            // convert JSON string to Map
            map = mapper.readValue(json, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Object x = map.get("query");
        String queryJson = gson.toJson(x);
        Map<String, String> queryMap = null;
        try {
            // convert JSON string to Map
            queryMap = mapper.readValue(queryJson, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String validationToken = queryMap.get("validationToken");
        logger.log("TOKEN: " + validationToken);
        String result = null;
        try {
			result = URLDecoder.decode( validationToken, "UTF-8" );
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        logger.log("TOKEN_DECODED: " + result);
        
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.TEXT_PLAIN);
//        ResponseEntity<String> response = new ResponseEntity<String>(result, headers, HttpStatus.OK);
//        return response;
        
//        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
//	    response.setIsBase64Encoded(false);
//	    response.setStatusCode(200);
//	    HashMap<String, String> headers = new HashMap<String, String>();
//	    headers.put("Content-Type", "text/plain");
//	    response.setHeaders(headers);
//	    response.setBody(validationToken);
//	    return response;
        
        return result;
	}
	
	// TODO create two methods handleNotification and handleValidation

}
