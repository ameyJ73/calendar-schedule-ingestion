import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;


public class NotificationHelper 
{
	static void processNotification(JSONObject changeNotification) throws JSONException, IOException
	{
		String subscriptionId = changeNotification.getString("subscriptionId");
		String changeType = changeNotification.getString("changeType");
		String resource = changeNotification.getString("resource");
		
		String userId = getUserId(resource);
		String accessToken = getAccessToken(userId);
		JSONObject event = HttpClientHelper.sendRequestToGraphAPI(accessToken, resource);
		String eventId = event.getString("id");
		
		if(!isEventReservedForAmazon(event)) return;
		else storeEvent(changeType, userId, subscriptionId, eventId, event);
	}
	
	private static JSONObject redeemRefreshToken(String refreshToken) throws IOException, JSONException
    {
    	JSONObject response = null;
    	String url = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
    	List<NameValuePair> urlParameters = getUrlParametersForRefreshToken(refreshToken);
    	response = HttpClientHelper.sendRequestToMsIdentityPlatform(url,urlParameters);
    	return response;
    }
	
	private static String getAccessToken(String userId) throws IOException, JSONException
	{
		String refreshToken = DynamoDBHelper.getRefreshToken(userId);
		JSONObject response = redeemRefreshToken(refreshToken);
		String accessToken = response.getString("access_token");
		String refreshTokenNew = response.getString("refresh_token");
		DynamoDBHelper.updateRefreshToken(userId, refreshTokenNew);
		return accessToken;
	}
	
	private static List<NameValuePair> getUrlParametersForRefreshToken(String refreshToken)
	{
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        
		urlParameters.add(new BasicNameValuePair("grant_type", "refresh_token"));
        urlParameters.add(new BasicNameValuePair("client_id", "e61a115b-92f3-4e7d-8109-52ea9d2c10a5"));
        urlParameters.add(new BasicNameValuePair("scope", "offline_access openid User.Read Calendars.Read"));
        urlParameters.add(new BasicNameValuePair("client_secret", "Ut2Km7Hd1w_FNG.JuU5WeBpP.~.e6k76j7"));
        urlParameters.add(new BasicNameValuePair("refresh_token", refreshToken));
		
        return urlParameters;
	}
	
	private static String getUserId(String resourceString)
	{
		String[] resourceStringParts = resourceString.split("/");
		return resourceStringParts[1];
	}
	
	private static boolean isEventReservedForAmazon(JSONObject event) throws JSONException
	{
		String subject = event.getString("subject");
		return subject.equalsIgnoreCase("Amazon");
	}
	
	private static void storeEvent(String changeType, String userId, String subscriptionId, String eventId, JSONObject event)
	{
		if(changeType.equals("created")) DynamoDBHelper.insertNewEvent(userId, subscriptionId, eventId, event.toString());
		else if(changeType.equals("updated")) DynamoDBHelper.updateExistingEvent(userId, subscriptionId, eventId, event.toString());
		else if(changeType.equals("deleted")) DynamoDBHelper.deleteExistingEvent(userId, subscriptionId, eventId, event.toString());
		return;
	}
	
}
