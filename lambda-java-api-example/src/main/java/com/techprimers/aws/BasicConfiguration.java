package com.techprimers.aws;

import java.io.IOException;
import java.util.Properties;

public class BasicConfiguration {
	
	private String clientId;
    private String authority;
    private String redirectUriSignin;
    private String redirectUriGraph;
    private String secretKey;
    private String msGraphEndpointHost;
    private String scopes;
    
    public BasicConfiguration()
    {
    	final Properties oAuthProperties = new Properties();
		try {
		    oAuthProperties.load(BasicConfiguration.class.getClassLoader().getResourceAsStream("oAuth.properties"));
		} catch (IOException e) {
		    System.out.println("Unable to read OAuth configuration. Make sure you have a properly formatted oAuth.properties file. See README for details.");
		    return;
		}
		
		this.clientId = oAuthProperties.getProperty("app.clientId");
		this.scopes = oAuthProperties.getProperty("app.scopes");
		this.authority = oAuthProperties.getProperty("app.authority");
		this.redirectUriSignin = oAuthProperties.getProperty("app.redirectUriSignin");
		this.redirectUriGraph = oAuthProperties.getProperty("app.redirectUriGraph");
		this.secretKey = oAuthProperties.getProperty("app.secretKey");
		this.msGraphEndpointHost = oAuthProperties.getProperty("app.msGraphEndpointHost");

    }
    
    public String getAuthority(){
        if (!authority.endsWith("/")) {
            authority += "/";
        }
        return authority;
    }

    public String getClientId() {
        return clientId;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRedirectUriSignin() {
        return redirectUriSignin;
    }

    public void setRedirectUriSignin(String redirectUriSignin) {
        this.redirectUriSignin = redirectUriSignin;
    }

    public String getRedirectUriGraph() {
        return redirectUriGraph;
    }

    public void setRedirectUriGraph(String redirectUriGraph) {
        this.redirectUriGraph = redirectUriGraph;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public void setMsGraphEndpointHost(String msGraphEndpointHost) {
        this.msGraphEndpointHost = msGraphEndpointHost;
    }

    public String getMsGraphEndpointHost(){
        return msGraphEndpointHost;
    }

	public String getScopes() {
		return scopes;
	}

	public void setScopes(String scopes) {
		this.scopes = scopes;
	}

}
