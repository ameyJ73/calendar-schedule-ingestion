package com.techprimers.aws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AuthHelper
{	
	private String clientId;
    private String clientSecret;
    private String authority;
    private String redirectUri;
    private String scopes;
    
    AuthHelper()
    {
    	BasicConfiguration configuration = new BasicConfiguration();
    	clientId = configuration.getClientId();
        authority = configuration.getAuthority();
        clientSecret = configuration.getSecretKey();
        redirectUri = configuration.getRedirectUriSignin();
        scopes = configuration.getScopes();
    }
    
    String extractAuthenticationCode(Map<String, Object> map)
    {
    	Gson gson = new GsonBuilder().setPrettyPrinting().create();
		ObjectMapper mapper = new ObjectMapper();
    	Object x = map.get("queryStringParameters");
    	
        String queryJson = gson.toJson(x);
        Map<String, String> queryMap = null;
        try {
            // convert JSON string to Map
            queryMap = mapper.readValue(queryJson, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String code = queryMap.get("code");
        
        return code;
    }
    
    JSONObject redeemAuthenticationCode(String code)
    {
    	JSONObject response = null;
    	String url = getUrlForTokenProcessing();
    	List<NameValuePair> urlParameters = getUrlParametersForAccessToken(code);
    	try {
			response = HttpClientHelper.sendRequestToMsIdentityPlatform(url,urlParameters);
		} catch (IOException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return response;
    }
    JSONObject redeemRefreshToken(String refreshToken)
    {
    	JSONObject response = null;
    	String url = getUrlForTokenProcessing();
    	List<NameValuePair> urlParameters = getUrlParametersForRefreshToken(refreshToken);
    	try {
			response = HttpClientHelper.sendRequestToMsIdentityPlatform(url,urlParameters);
		} catch (IOException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return response;
    }
    
    private String getUrlForTokenProcessing()
    {
    	String url = this.authority + "oauth2/v2.0/token";
    	return url;
    }
    private List<NameValuePair> getUrlParametersForAccessToken(String code)
	{
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        
		urlParameters.add(new BasicNameValuePair("grant_type", "authorization_code"));
        urlParameters.add(new BasicNameValuePair("client_id", this.clientId));
        urlParameters.add(new BasicNameValuePair("redirect_uri", this.redirectUri));
        urlParameters.add(new BasicNameValuePair("client_secret", this.clientSecret));
        urlParameters.add(new BasicNameValuePair("code", code));
		
        return urlParameters;
	}
    private List<NameValuePair> getUrlParametersForRefreshToken(String refreshToken)
	{
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        
		urlParameters.add(new BasicNameValuePair("grant_type", "refresh_token"));
        urlParameters.add(new BasicNameValuePair("client_id", this.clientId));
        urlParameters.add(new BasicNameValuePair("scope", this.scopes));
        urlParameters.add(new BasicNameValuePair("client_secret", this.clientSecret));
        urlParameters.add(new BasicNameValuePair("refresh_token", refreshToken));
		
        return urlParameters;
	}
    
}
