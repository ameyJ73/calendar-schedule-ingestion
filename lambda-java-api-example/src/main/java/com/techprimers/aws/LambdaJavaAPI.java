package com.techprimers.aws;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;


import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.microsoft.graph.auth.confidentialClient.AuthorizationCodeProvider;
import com.microsoft.graph.auth.confidentialClient.ClientCredentialProvider;
import com.microsoft.graph.auth.enums.NationalCloud;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.models.extensions.Subscription;
import com.microsoft.graph.requests.extensions.GraphServiceClient;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;


public class LambdaJavaAPI implements RequestHandler<Object, APIGatewayProxyResponseEvent>
{   
	public APIGatewayProxyResponseEvent handleRequest(Object event, Context context)
	{
		AuthHelper authHelper = new AuthHelper();
		DynamoDBHelper.initDynamoDbClient();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LambdaLogger logger = context.getLogger();
		
		logger.log("EVENT: " + gson.toJson(event));
		logger.log("CONTEXT: " + gson.toJson(context));
		

		// process event
		Map<String, Object> eventMap = processEventObject(event);
		String code = authHelper.extractAuthenticationCode(eventMap);
		
	
//		// process authentication code
//        JSONObject authResponse = authHelper.redeemAuthenticationCode(code);
//        String accessToken = null;
//        String refreshToken = null;
//		try {
//			accessToken = authResponse.getString("access_token");
//	        refreshToken = authResponse.getString("refresh_token");
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        logger.log("ACCESS_TOKEN: " + accessToken);
//        logger.log("REFRESH_TOKEN: " + refreshToken);
//        
//        
//        // get user info (testing)
//        JSONObject graphResponse = null;
//		try {
//			graphResponse = HttpClientHelper.sendRequestToGraphAPI(accessToken, "v1.0/me");
//		} catch (IOException | JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        logger.log("USER_DATA: " + graphResponse);
//        String userId = null;
//        try {
//			userId = graphResponse.getString("id");
//		} catch (JSONException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//        
//        DynamoDBHelper.persistData(userId, refreshToken);
//        
//        // get new access token
//        authResponse = authHelper.redeemRefreshToken(refreshToken);
//        accessToken = null;
//        refreshToken = null;
//		try {
//			accessToken = authResponse.getString("access_token");
//	        refreshToken = authResponse.getString("refresh_token");
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        logger.log("ACCESS_TOKEN_NEW: " + accessToken);
//        logger.log("REFRESH_TOKEN_NEW: " + refreshToken);
//        
        
//        List<String> scopes = Arrays.asList("https://graph.microsoft.com/calendars.read");
//        AuthorizationCodeProvider authProvider = new AuthorizationCodeProvider(
//        		"e61a115b-92f3-4e7d-8109-52ea9d2c10a5", 
//        		scopes, 
//        		code, 
//        		"https://nst8ejvec4.execute-api.us-east-1.amazonaws.com/prod/lambda-api-java-example/", 
//        		NationalCloud.Global, 
//        		"common", 
//        		"Ut2Km7Hd1w_FNG.JuU5WeBpP.~.e6k76j7");
//        
//        IGraphServiceClient graphClient = GraphServiceClient
//                .builder()
//                .authenticationProvider(authProvider)
//                .buildClient();
//        
//		graphClient.setServiceRoot("https://graph.microsoft.com/beta");
//		Subscription subscription = new Subscription();
//		subscription.changeType = "created";
//		subscription.notificationUrl = "https://nst8ejvec4.execute-api.us-east-1.amazonaws.com/prod/notification";
//		subscription.resource = "/users/822c7a22d36757f8/events";
//		subscription.expirationDateTime = Calendar.getInstance();
//		subscription.clientState = "secretClientValue";
//		
//		subscription.expirationDateTime.add(Calendar.HOUR, 1);
//		
//		logger.log("SUBSCRIPTION: " + subscription.toString());
//        
//		subscription = graphClient.subscriptions()
//	            .buildRequest()
//	            .post(subscription);
//		String response = subscription.id;
//		
//		logger.log("RESPONSE: " + response);

	    return getResponse(code,"chala");
    }
	
	private Map<String, Object> processEventObject(Object event)
    {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		ObjectMapper mapper = new ObjectMapper();
		
        String json = gson.toJson(event);
        Map<String, Object> map = null;
        try {
            // convert JSON string to Map
            map = mapper.readValue(json, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return map;
    }
	private APIGatewayProxyResponseEvent getResponse(String accessToken, String message)
	{
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
	    response.setIsBase64Encoded(false);
	    response.setStatusCode(200);
	    HashMap<String, String> headers = new HashMap<String, String>();
	    headers.put("Content-Type", "text/html");
	    response.setHeaders(headers);
	    response.setBody("<!DOCTYPE html><html><head><title>AWS Lambda sample</title></head><body>"+
	    	      "<h1>Calendar Schedule Ingestion</h1><p>Thank You. You have been registered.</p>" +
	    	      "<p>Access Token : " +
	    	      accessToken +
	    	      "</p><p>Subscription Response : " +
	    	      message +
	    	      "</p></body></html>");
	    return response;
	}
	
}