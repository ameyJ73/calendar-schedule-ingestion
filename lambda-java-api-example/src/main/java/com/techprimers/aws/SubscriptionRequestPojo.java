package com.techprimers.aws;
public class SubscriptionRequestPojo {
	public String changeType;
	public String notificationUrl;
	public String resource;
	public String expirationDateTime;
	public String clientState;
}
