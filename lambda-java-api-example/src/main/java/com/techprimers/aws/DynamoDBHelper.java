package com.techprimers.aws;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;

public class DynamoDBHelper {
	private static DynamoDB dynamoDb;
    private static String DYNAMODB_TABLE_NAME = "user-token-data";
    private static Regions REGION = Regions.US_EAST_1;
    
    static void initDynamoDbClient() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setRegion(Region.getRegion(REGION));
        dynamoDb = new DynamoDB(client);
    }
	static PutItemOutcome persistData(String userId, String refreshToken) 
	{
		  Table table = dynamoDb.getTable(DYNAMODB_TABLE_NAME);
		  PutItemOutcome outcome = table.putItem(new PutItemSpec().withItem(
		    new Item().withString("user-id", userId)
		               .withString("refresh-token", refreshToken)));
		  return outcome;
	}

}
